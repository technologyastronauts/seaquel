
# Overview

This library deals with generation of SQL. What one would like to have is a
programmatic interface to SQL that allows to create a query and turn it into
SQL.

~~~ruby
  include Seaquel # to avoid having to write Seaquel.select - optional

  select.from(table('foobar')).where(column('name').eq('baz')).
    to_sql # => %Q(SELECT * FROM "foobar" WHERE "name"='baz')
~~~

As can be seen from the above snippet, SQL construction uses a DSL that is functional in nature. The DSL needs to support all kinds of constructs and cover a broad range of SQL syntax. 

~~~ruby
  update(table('bar')).set(literal('a=1'), column('b').to(2)).
    to_sql # => %Q(UPDATE "bar" SET a=1, "b"=2)
~~~

~~~ruby
  insert.fields(column('a'), column('b')).values('1', 2).
    into(table('baz')).
    to_sql # => %Q(INSERT INTO "baz" ("a", "b") VALUES ('1', 2))
~~~

~~~ruby
  delete.from(table('baz')).
    where(column('a').eq(1)).
    join(table('bar')).on(column('b').eq(column('c'))).
    to_sql # => %Q(DELETE FROM "baz" INNER JOIN "bar" ON "b"="c" WHERE "a"=1)
~~~

As you can see, method invocation can be in any order and the generator figures out what you mean.

# Different Types of JOINs

# Positional (Binding) Parameters

To bind a value in your client, use the `binding(position)` macro. The returned object can be treated as part of an expression. 

~~~ruby
  select.where(column('a').eq(binding(1))).
    to_sql # => %Q(SELECT * WHERE "a"=$1)
~~~

# SQL Function Calls

To formulate something like `SELECT string_agg(a) FROM table`, you need to be able to generate function calls. 

~~~ruby
  select.project(funcall('string_agg', column(:a))).from(table(:table)).
    to_sql # => %Q(SELECT string_agg("a") FROM "table")
~~~

The funcall method call can be wrapped inside a Ruby function, yielding a quite natural interface: 

~~~ruby
  def string_agg col
    funcall('string_agg', column(col))
  end
  
  select.project(string_agg(:a)).from(table(:table)).
    to_sql # => %Q(SELECT string_agg("a") FROM "table")
~~~

# SELECT

Here's how you access columns from a given table. 

~~~ruby
  users = table('users')
  select.project(users['id'], users['name']).from(users).
    to_sql # => %Q(SELECT "users"."id", "users"."name" FROM "users")
~~~

Also, table aliases are possible and can shorten the issued statements quite a bit.

~~~ruby
  users = table('users').as('u')
  select.project(users['id'], users['name']).from(users).
    to_sql # => %Q(SELECT "u"."id", "u"."name" FROM "users" AS "u")
~~~

Here's how you set an offset and a limit.

~~~ruby
  select.limit(5).offset(3).
    to_sql # => %Q(SELECT * OFFSET 3 LIMIT 5)
~~~


# INSERT

PostgreSQL allows inserting multiple rows at once:

~~~ruby
  insert.into(table('foo')).fields(column('a'), column('b')).
    values(1, 2).
    values(2, 3).
    to_sql # => %Q(INSERT INTO "foo" ("a", "b") VALUES (1, 2), (2, 3))
~~~
