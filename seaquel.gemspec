# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name     = "seaquel"
  s.version  = '0.2'
  s.summary  = "A DSL for SQL."
  s.email    = "kaspar.schiess@technologyastronauts.ch"
  s.homepage = "https://bitbucket.org/technologyastronauts/seaquel"
  s.authors  = ['Kaspar Schiess', 'Florian Hanke']
  
  s.description = <<-EOF
    Generates SQL from Ruby code. 
  EOF

  # s.add_runtime_dependency 'aggregate', '~> 0.2', '>= 0.2.2'
  
  s.files         = %w(FEATURES README LICENSE) + Dir['lib/**/*']
  s.test_files    = Dir['qed/**/*'] + Dir['spec/**/*']
  s.require_paths = ["lib"]

  s.licenses      = 'MIT'
end
