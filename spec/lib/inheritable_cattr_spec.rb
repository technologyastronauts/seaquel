

require 'seaquel/inheritable_cattr'

describe Seaquel::InheritableCattr do
  # a <-- b
  #    `- c
  let(:a) { Class.new }
  let(:b) { Class.new(a) }
  let(:c) { Class.new(a) }

  before(:each) do
    a.extend described_class

    a.property :foo, []
  end

  it 'allows accessing the property on the class' do
    a.foo << 1
    a.foo.assert == [1]
  end
  it 'allows accessing the property on the subclass' do
    b.foo << 1
    b.foo.assert == [1]
    a.foo.assert == []
    c.foo.assert == []
  end
  it 'allows extending the values set in the superclass' do
    a.foo << 1
    b.foo << 2

    a.foo.assert == [1]
    b.foo.assert == [1, 2]
  end
end