
require 'spec_helper'

describe Seaquel::ExpressionConverter do
  include Seaquel

  let(:quoter) { Seaquel::Quoter.new }
  let(:ec) { Seaquel::ExpressionConverter.new(quoter) }

  it 'converts arrays by iterating on them' do
    ec.sql([1,2,3]).assert == '(1, 2, 3)'
  end
  it 'converts ranges' do
    ec.visit_binop(:in, "left", 1..10).assert == "'left' BETWEEN 1 AND 10"
  end

  describe 'expressions' do
    it '= binds higher than AND' do
      exp = column(:a).eq(1).and(column(:b).eq(2))
      ec.sql(exp).assert == '"a"=1 AND "b"=2'
    end
  end
end