
require 'spec_helper'

describe Seaquel::AST::Expression do
  let(:q) { Seaquel::Quoter.new }
  let(:ec) { Seaquel::ExpressionConverter.new(q) }

  # ----------------------------------------------------------------------------
  # Using a, b and c (all columns) or e1, e2 and e3 (any expression), various 
  # patterns are formulated and tested against SQL that should be aequivalent.
  #
  extend Seaquel

  a, b, c = [:a, :b, :c].map { |e| column(e) }

  e1 = Seaquel::AST::Expression.new
  e2 = Seaquel::AST::Expression.new
  e3 = Seaquel::AST::Expression.new

  # Define an ad-hoc #visit method for each of those expressions, so that we
  # can test operators in general: 
  [e1, e2, e3].each_with_index do |e, i|
    e.define_singleton_method(:visit) do |visitor|
      Seaquel::Bit.new("e#{i+1}", :inf)
    end
  end

  # Here are the test cases:
  [
    # basic column AND/OR
    [a & b, '"a" AND "b"'],             # 1, 
    [a | b, '"a" OR "b"'], 
    # basic expression AND/OR
    [e1 & e2, 'e1 AND e2'],
    [e1 | e2, 'e1 OR e2'],
    [e1 & e2 | e3, 'e1 AND e2 OR e3'],  # 5
    [(e1 & e2) | e3, 'e1 AND e2 OR e3'],
    [e1 & (e2 | e3), 'e1 AND (e2 OR e3)'],
    # Basic algebra
    [e1 + e2, 'e1+e2'],
    [e1 - e2, 'e1-e2'],
    [e1 * e2, 'e1*e2'],                 # 10
    [e1 / e2, 'e1/e2'],                 
    # NOT
    [!e1, 'NOT e1'],
    # Equalities, Inequalities
    [e1 != e2, 'e1!=e2'],
    [e1 == e2, 'e1=e2'],
    [e1 < e2, 'e1<e2'],                 # 15
    [e1 > e2, 'e1>e2'],
    [e1 <= e2, 'e1<=e2'],
    [e1 >= e2, 'e1>=e2'],
    # Mixed bag
    [(e1.not) == e2, '(NOT e1)=e2'],
    [(!e1) == e2, '(NOT e1)=e2'],       # 20
    [!e1 == e2, '(NOT e1)=e2'],
    # More algebra
    [e1 % e2, 'e1%e2'],
    [e1 ** e2, 'e1^e2'],
    [e1.sqrt(e2), 'e1|/e2'],
    [e1.cbrt(e2), 'e1||/e2'],           # 25
    [e1.fact, 'e1!'],                   
    # Bitwise operators: more rare and thus more verbose.
    [e1.bitand(e2), 'e1&e2'],
    [e1.bitor(e2), 'e1|e2'],
    [e1.bitxor(e2), 'e1#e2'],
    [e1.bitnot, '~e1'],                 # 30
    # Bit shifts
    [e1 << e2, 'e1<<e2'],
    [e1 >> e2, 'e1>>e2'],
    # Precedences
    [(e1+e2)*e3, '(e1+e2)*e3'],         
    # Inclusion
    [e1.in([1, 2, 3]), 'e1 IN (1, 2, 3)'], 
    [e1.in(1..5), 'e1 BETWEEN 1 AND 5'], 
    [e1.in(Set.new([1, 2, 3])), 'e1 IN (1, 2, 3)'],   

  ].each_with_index do |(exp, sql), idx|
    begin
      it "line #{"%2d:"%(idx+1)} turns into #{sql}" do
        ec.sql(exp).toplevel.assert == sql
      end
    rescue Exception => ex
      ex.set_backtrace(["example in spec at line #{idx+1}", *ex.backtrace])
      raise ex
    end
  end

  it 'has a list of defined operations, including unary, binary and join operations' do
    klass = Seaquel::AST::Expression

    not_empty = -> (l) { l.size.assert > 0 }

    (klass.operations & klass.unaryops).assert not_empty
    (klass.operations & klass.joinops).assert not_empty
    (klass.operations & klass.binops).assert not_empty

    klass.unaryops.assert not_empty
    klass.joinops.assert not_empty
    klass.binops.assert not_empty
  end
end