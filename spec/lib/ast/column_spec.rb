
require 'spec_helper'

describe Seaquel::AST::Column do
  include Seaquel

  it 'allows comparison ">"' do
    select.where(column('foo').gt(1)).generates <<-SQL
      SELECT * WHERE "foo">1
    SQL
  end
  it 'allows comparison ">="' do
    select.where(column('foo').gteq(1)).generates <<-SQL
      SELECT * WHERE "foo">=1
    SQL
  end
  it 'allows comparison "<"' do
    select.where(column('foo').lt(1)).generates <<-SQL
      SELECT * WHERE "foo"<1
    SQL
  end
  it 'allows comparison "<="' do
    select.where(column('foo').lteq(1)).generates <<-SQL
      SELECT * WHERE "foo"<=1
    SQL
  end
  it 'allows comparison "!="' do
    select.where(column('foo').noteq(1)).generates <<-SQL
      SELECT * WHERE "foo"!=1
    SQL
  end
  it 'allows comparison "IS"' do
    select.where(column('foo').is(1)).generates <<-SQL
      SELECT * WHERE "foo" IS 1
    SQL
  end
  it 'allows comparison "IS NOT"' do
    select.where(column('foo').isnot(1)).generates <<-SQL
      SELECT * WHERE "foo" IS NOT 1
    SQL
  end
  it 'allows comparison "IN"' do
    select.where(column('foo').in([1,2])).generates <<-SQL
      SELECT * WHERE "foo" IN (1, 2)
    SQL
  end

  it 'allows ASC modifier for use in ORDER BY' do
    select.order_by(column('foo').asc).generates <<-SQL
      SELECT * ORDER BY "foo" ASC
    SQL
  end
  it 'allows DESC modifier for use in ORDER BY' do
    select.order_by(column('foo').desc).generates <<-SQL
      SELECT * ORDER BY "foo" DESC
    SQL
  end

  describe 'string comparison functions' do
    it 'allows comparison "LIKE"' do
      select.where(column('foo').like('test')).generates <<-SQL
        SELECT * WHERE "foo" LIKE 'test'
      SQL
    end    
    it 'allows comparison "NOT LIKE"' do
      select.where(column('foo').not_like('test')).generates <<-SQL
        SELECT * WHERE "foo" NOT LIKE 'test'
      SQL
    end    
    it 'allows comparison "SIMILAR TO"' do
      select.where(column('foo').similar_to('test')).generates <<-SQL
        SELECT * WHERE "foo" SIMILAR TO 'test'
      SQL
    end    
    it 'allows comparison "NOT SIMILAR TO"' do
      select.where(column('foo').not_similar_to('test')).generates <<-SQL
        SELECT * WHERE "foo" NOT SIMILAR TO 'test'
      SQL
    end    
    it 'allows comparison "ILIKE"' do
      select.where(column('foo').ilike('test')).generates <<-SQL
        SELECT * WHERE "foo" ILIKE 'test'
      SQL
    end    
    it 'allows comparison "NOT ILIKE"' do
      select.where(column('foo').not_ilike('test')).generates <<-SQL
        SELECT * WHERE "foo" NOT ILIKE 'test'
      SQL
    end    
  end
end