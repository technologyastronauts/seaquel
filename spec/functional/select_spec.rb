
require 'spec_helper'

describe 'SELECT statements' do
  include Seaquel

  it 'allows FROM clause' do
    select.from(table('foo')).generates <<-SQL
      SELECT * FROM "foo"
    SQL
  end
  it 'allows bare selection' do
    select.project(1).generates <<-SQL
      SELECT 1
    SQL
  end
  it 'allows bare selection list' do
    select.project(1, 2, 3).generates <<-SQL
      SELECT 1, 2, 3
    SQL
  end

  describe '#group_by' do
    it 'works by replacing earlier group_bys' do
      a = column(:a)
      b = column(:b)
      select.group_by(a).group_by(b).generates <<-SQL
        SELECT * GROUP BY "b"
      SQL
    end
  end
  describe '#having' do
    it 'works like #where' do
      select.having(1).having(2).generates <<-SQL
        SELECT * HAVING 1 AND 2
      SQL
    end
  end
  describe '#join' do
    it 'allows joining tables (INNER JOIN being the default)' do
      foo = table(:foo)
      bar = table(:bar)
      select.from(foo).join(bar).on(foo[:id].eq(bar[:foo_id])).generates <<-SQL
        SELECT * FROM "foo" INNER JOIN "bar" ON "foo"."id"="bar"."foo_id"
      SQL
    end
  end
  describe '#order_by' do
    it 'allows ordering the query' do
      select.order_by(column(:a)).generates <<-SQL
        SELECT * ORDER BY "a"
        SQL
    end
    it 'overrides previous order_bys' do
      select.order_by(column(:a)).order_by(column(:b)).generates <<-SQL
        SELECT * ORDER BY "b"
      SQL
    end
  end
  describe '#project' do
    it 'allows expressions in project' do
      select.project(literal(1).as('one')).generates <<-SQL
        SELECT 1 AS "one"
      SQL
    end
    it 'overwrites any earlier projects (for flexibility)' do
      select.project(column('a')).project(column('b')).generates <<-SQL
        SELECT "b"
      SQL
    end
  end
  describe '#where' do
    it 'allows simple literal WHERE clause' do
      select.where(literal('1=2')).generates <<-SQL
        SELECT * WHERE 1=2
      SQL
    end
    it 'joins literals with correct precedence' do
      select.where(literal('1=2'), literal('2=3')).generates <<-SQL
        SELECT * WHERE 1=2 AND 2=3
      SQL
    end
    it 'allows more complex WHERE clause' do
      select.where(column('name').eq('baz')).generates <<-SQL
        SELECT * WHERE "name"='baz'
      SQL
    end

    describe 'subselects' do
      it 'are allowed' do
        sub = select.
          from(table('foo')).
          project(column('id')).
          where(column('count').gt(10))

        select.from(table("bar")).where(column('id').eq(sub)).generates <<-SQL
          SELECT * FROM "bar" WHERE "id"=(SELECT "id" FROM "foo" WHERE "count">10) 
        SQL
      end
    end

    describe 'multiple where() calls' do
      it 'are joined using AND' do
        select.where(true).where(false).generates <<-SQL
          SELECT * WHERE TRUE AND FALSE
        SQL
      end
      it 'are joined using AND (arglist)' do
        select.where(true, false).generates <<-SQL
          SELECT * WHERE TRUE AND FALSE
        SQL
      end
    end
  end
end