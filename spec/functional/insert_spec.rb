
require 'spec_helper'

describe 'INSERT statements' do
  include Seaquel

  it 'basic functionality' do
    insert.into(table('foo')).generates <<-SQL
      INSERT INTO "foo"
    SQL
  end
  it 'an actual statement' do
    insert.
      fields(column('a'), column('b')).
      values('1', 2).into(table('baz')).
      generates <<-SQL
        INSERT INTO "baz" ("a", "b") VALUES ('1', 2)
      SQL
  end
  it 'omitting the fields part' do
    insert.values(1, 2).into(table('bar')).generates <<-SQL
      INSERT INTO "bar" VALUES (1, 2)
    SQL
  end
  it 'allows usage of columns linked to a table' do
    foo = table('foo')
    insert.into(foo).fields(foo['bar']).values(1).generates <<-SQL
      INSERT INTO "foo" ("bar") VALUES (1)
    SQL
  end
end