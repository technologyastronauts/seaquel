
require 'spec_helper'

describe 'DELETE statements' do
  include Seaquel

  it 'basic' do
    delete.from(table('bar')).generates <<-SQL
      DELETE FROM "bar"
    SQL
  end
  it 'initial example from QED' do
    delete.
      from(table('baz')).where(column('a').eq(1)).
      join(table('bar')).on(column('b').eq(column('c'))).generates <<-SQL
        DELETE FROM "baz" INNER JOIN "bar" ON "b"="c" WHERE "a"=1 
      SQL
  end
end