
require 'spec_helper'

describe 'UPDATE statements' do
  include Seaquel

  it 'can be used' do
    update(table('bar')).set(column('foo').to(1)).generates <<-SQL
      UPDATE "bar" SET "foo"=1
    SQL
  end
  it 'set statements can be chained' do
    update(table('bar')).
      set(column('foo').to(1)).
      set(column('bar').to(2)).generates <<-SQL
      UPDATE "bar" SET "foo"=1, "bar"=2
    SQL
  end
  it 'works using the table[column] syntax' do
    foo = table('foo')
    update(foo).set(foo['bar'].to(1)).generates <<-SQL
      UPDATE "foo" SET "bar"=1
    SQL
  end
  it 'allows WHERE clauses' do
    update(table('foo')).where(column('id').eq(1)).generates <<-SQL
      UPDATE "foo" WHERE "id"=1
    SQL
  end
end