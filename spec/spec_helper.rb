
require 'seaquel'
require 'ae'

RSpec.configure do |config|
end

module GeneratesHelper
  def generates sql
    self.to_sql.assert == sql.strip
  end
end

Seaquel::AST::Node.send(:include, GeneratesHelper)
