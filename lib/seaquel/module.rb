

module Seaquel
  module_function

  def select
    AST::Node.new(:select)
  end
  def update table
    AST::Node.new(:update, table)
  end
  def insert
    AST::Node.new(:insert)
  end
  def delete
    AST::Node.new(:delete)
  end

  def column name
    AST::Column.new(name)
  end

  def table name
    AST::Table.new(name)
  end

  def literal text
    AST::Literal.new(text)
  end
  def binding position
    AST::Binding.new(position)
  end
  def funcall name, *args
    AST::Funcall.new(name, args)
  end
end