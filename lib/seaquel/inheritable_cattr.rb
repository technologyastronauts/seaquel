

module Seaquel
  module InheritableCattr
    def property name, default_value=nil
      getter = name
      setter = "#{name}="
      local_var = "@_#{name}_local"

      get = -> { instance_variable_get(local_var) }
      set = -> (value) { instance_variable_set(local_var, value) }

      define_singleton_method(name) do
        # Retrieve our current value
        value = instance_exec(&get)

        # Retrieve a value from defaults
        from_super = default_value

        # Retrieve value from superclass (maybe)
        if superclass.respond_to?(name)
          from_super = superclass.send(name)
        end

        # Accessing the value creates an instance here. If it did not exist 
        # previously, it does now. 
        unless value
          value = from_super.dup
          instance_exec(value, &set)
        end

        value || from_super.dup
      end

    end
  end
end

