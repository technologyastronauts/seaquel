

module Seaquel
  class Statement
    # A join clause inside an SQL statement. 
    class Join
      attr_reader :tables
      attr_reader :ons

      def initialize tables
        @tables = AST::List.new(tables)

        @ons = AST::JoinOp.new(:and, [])
      end

      def on *exps
        ons.concat(*exps)
      end

      # Turns a join statement into SQL by invoking conversion_helper on the
      # right parts.
      #
      # @param conversion_helper [#convert] instance of a class that implements
      #   a #convert method that turns expressions into SQL. 
      # @return [String] SQL string for this join
      #
      def convert conversion_helper
        parts = []
        parts << 'INNER JOIN'
        parts << conversion_helper.convert(tables)
        parts << 'ON'
        parts << conversion_helper.convert(ons)
      end
    end
  end
end