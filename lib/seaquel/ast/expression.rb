
require_relative 'has_ops'

module Seaquel::AST
  # Base class for all expression-type AST nodes. 
  #
  class Expression
    extend HasOps

    binop :eq, :==
    binop :noteq, :"!="
    
    binop :lt, :<
    binop :lteq, :<=

    binop :gt, :>
    binop :gteq, :>=

    binop :is
    binop :isnot

    binop :in

    binop :like
    binop :not_like
    binop :ilike
    binop :not_ilike
    binop :similar_to
    binop :not_similar_to

    binop :plus, :+
    binop :minus, :-
    binop :times, :*
    binop :div, :/
    binop :mod, :%

    binop :pow, :**
    binop :sqrt
    binop :cbrt

    binop :bitand
    binop :bitor
    binop :bitxor

    binop :sleft, :<<
    binop :sright, :>>

    joinop :and, :&
    joinop :or, :|

    unaryop :not, :!
    unaryop :fact
    unaryop :bitnot

    def as name
      Alias.new(name, self)
    end

    def asc
      Order.new(:asc, self)
    end
    def desc
      Order.new(:desc, self)
    end

    def lisp_inspect type, *args
      "(" + 
        [type.to_s, args.map(&:inspect)].join(' ') + ")"
    end
  end
end