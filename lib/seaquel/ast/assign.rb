module Seaquel::AST
  # Assignment of right to left. 
  # This class can be visited.
  #
  class Assign
    attr_reader :left, :right

    def initialize left, right
      @left, @right = left, right
    end

    def visit visitor
      visitor.visit_assign(left, right)
    end
  end
end

