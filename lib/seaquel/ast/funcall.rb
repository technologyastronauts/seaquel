
require_relative 'expression'

module Seaquel::AST
  class Funcall < Expression
    attr_reader :name, :args

    def initialize name, args
      @name = name
      @args = args
    end

    def visit visitor
      visitor.visit_funcall(name, args)
    end
  end
end