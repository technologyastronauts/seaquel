
module Seaquel::AST
  class Unary < Expression
    attr_reader :exp
    attr_reader :op

    def initialize op, exp
      @exp = exp
      @op = op
    end

    def visit visitor
      visitor.visit_unary op, exp
    end

    def inspect
      "(#{op.inspect} #{exp.inspect})"
    end
  end
end