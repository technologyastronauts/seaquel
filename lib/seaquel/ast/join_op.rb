
require 'forwardable'

module Seaquel::AST
  # A binary operation that can be used to join things together,  like 'AND' or
  # 'OR'.
  #
  class JoinOp < Expression
    extend Forwardable

    attr_reader :op
    attr_reader :elements

    def initialize op, elements=[]
      @op, @elements = op, elements
    end

    def concat *more
      more.each do |el|
        elements << el
      end
    end

    def empty?
      elements.empty?
    end

    def visit visitor
      visitor.visit_joinop(op, elements)
    end

    def inspect
      lisp_inspect(:join_op, op, elements)
    end
  end
end