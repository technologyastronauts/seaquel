
module Seaquel::AST
  # Special case of a column list that needs to be formatted without table
  # references.
  #
  class ColumnList < List
    def visit visitor
      visitor.visit_column_list(list)
    end
  end
end