
require_relative 'expression'

module Seaquel::AST
  class Literal < Expression
    attr_reader :text

    def initialize text
      @text = text
    end

    def visit visitor
      visitor.visit_literal(text)
    end
  end
end