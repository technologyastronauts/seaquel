
require_relative 'expression'

module Seaquel::AST
  class Column < Expression
    attr_reader :name
    attr_reader :table

    def initialize name, table=nil
      @name = name
      @table = table
    end

    # Set the column to value. 
    #
    def to value
      Assign.new(self, value)
    end

    # Visits the column as part of sql generation. 
    #
    def visit visitor
      visitor.visit_column self
    end

    # Returns an SQL column reference, including the table name. 
    #
    def as_full_reference quoter
      parts = []

      if table
        parts << table.as_column_prefix(quoter)
      end

      parts << as_column_reference(quoter)

      parts.join('.')
    end

    # Returns an SQL column reference, excluding table name. 
    #
    def as_column_reference quoter
      quoter.column(name)
    end

    def inspect
      if table
        lisp_inspect(:column, name, table)
      else
        lisp_inspect(:column, name)
      end
    end
  end
end