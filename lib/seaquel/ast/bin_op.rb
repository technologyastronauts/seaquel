
module Seaquel::AST
  # A binary statement as in left=right. 
  # This class can be visited.
  #
  class BinOp < Expression
    attr_reader :op, :left, :right
    
    def initialize op, left, right
      @op, @left, @right = op, left, right
    end

    def visit visitor
      visitor.visit_binop(op, left, right)
    end

    def inspect 
      "(" + ['bin_op', op.inspect, left.inspect, right.inspect].join(' ') + ")"
    end
  end
end
