
require 'forwardable'

module Seaquel::AST
  class List
    extend Forwardable

    attr_reader :list

    def initialize initial=[]
      @list = initial
    end

    # Behaves array-like in many respects
    def_delegators :@list, :empty?, :concat, :size, :<<, :map, :replace

    def visit visitor
      visitor.visit_list(list)
    end

    def inspect
      '[' + list.map { |e| e.inspect }.join(', ') + ']'
    end
  end
end