
require_relative 'expression'

module Seaquel::AST
  class Binding < Expression
    attr_reader :pos

    def initialize pos
      @pos = pos
    end

    def visit visitor
      visitor.visit_binding pos.to_s
    end

    def inspect
      "(bind #{pos})"
    end
  end
end