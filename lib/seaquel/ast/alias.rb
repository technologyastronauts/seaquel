
module Seaquel::AST
  class Alias
    attr_reader :name, :expression

    def initialize name, expression
      @name = name
      @expression = expression
    end

    def visit visitor
      visitor.visit_alias(name, expression)
    end

    def inspect
      "(alias #{name.inspect} #{expression.inspect})"
    end
  end
end