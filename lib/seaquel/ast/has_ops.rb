
require 'seaquel/inheritable_cattr'

module Seaquel::AST
  module HasOps
    def self.extended base
      base.instance_eval do
        extend Seaquel::InheritableCattr

        property :binops, []
        property :unaryops, []
        property :joinops, []
      end
    end

    def operations
      binops | joinops | unaryops
    end

    def binop op, *aliases
      binops << op
      binops.concat(aliases)

      define_method(op) do |exp|
        BinOp.new(op, self, exp)
      end

      aliases.each do |op_alias|
        alias_method(op_alias, op)
      end
    end
    def joinop op, *aliases
      joinops << op
      joinops.concat(aliases)

      define_method(op) do |exp|
        JoinOp.new(op, [self, exp])
      end

      aliases.each do |op_alias|
        alias_method(op_alias, op)
      end
    end
    def unaryop op, *aliases
      unaryops << op
      unaryops.concat(aliases)

      define_method(op) do
        Unary.new(op, self)
      end

      aliases.each do |op_alias|
        alias_method(op_alias, op)
      end
    end
  end
end