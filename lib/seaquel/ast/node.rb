module Seaquel::AST
  # A general purpose node containing a node type and a list of arguments. 
  # The node stores a link to its parent or nil if no such parent exists. 
  # This class can be visited.
  #
  class Node
    attr_reader :type
    attr_reader :parent
    attr_reader :args

    def initialize *args
      if args.first.kind_of?(Symbol)
        @type, *@args = args
      else
        @parent, @type, *@args = args
      end
    end

    # Replaces previous projection list with this one. 
    #
    # @param list [Array<Column>]
    # @return [Node] node for chaining
    #
    def project *fields
      node(:project, fields)
    end

    def from *tables
      node(:from, *tables)
    end

    def where *exps
      node(:where, JoinOp.new(:and, exps))
    end

    def set *assign_list
      node(:set, assign_list)
    end

    def join *tables
      node(:join, tables)
    end
    def on *exps
      node(:on, exps)
    end

    def limit n
      node(:limit, n)
    end
    def offset n
      node(:offset, n)
    end

    def having *exps
      node(:having, exps)
    end
    def group_by *fields
      node(:group_by, fields)
    end

    # Replaces previous ORDER BY specification with this one. 
    #
    # @param list [Array<Column>]
    # @return [Node] node for chaining
    #
    def order_by *list
      node(:order_by, list)
    end

    # INSERT INTO ... (FIELDS) VALUES (...)
    def fields *list
      node(:fields, list)
    end
    
    # Adds a values list to an INSERT statement. You have to pass all the columns
    # at once; repeating this method call will create an INSERT statement that
    # inserts multiple rows at once (a Postgres extension).
    #
    # Example: 
    #   include Seaquel
    #   insert.into(table('foo')).values(1, 2)
    #   # => INSERT INTO "foo" VALUES (1, 2)
    #
    # @param list [Array<AST::Expression>] values to insert
    # @return [AST::Node] query you are building
    # 
    def values *list
      node(:values, list)
    end

    def into table
      node(:into, table)
    end

    def node *args
      self.class.new(self, *args)
    end

    def visit visitor
      method_name = "visit_#{type}"
      if visitor.respond_to?(:visit_node)
        visitor.visit_node(self)
      else
        visitor.send(method_name, parent, *args)
      end
    end

    def to_sql
      ::Seaquel::Generator.new(self).compact_sql
    end

    def inspect
      [type, args, parent].inspect
    end
  end
end