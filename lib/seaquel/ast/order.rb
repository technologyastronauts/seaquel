

module Seaquel::AST
  class Order
    attr_reader :order, :column

    def initialize order, column
      @order = order
      @column = column
    end

    def visit visitor
      visitor.visit_order(order, column)
    end
  end
end