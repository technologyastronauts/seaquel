module Seaquel::AST
  class Table < Expression
    attr_reader :name

    def initialize name
      @name = name
    end

    def visit visitor
      visitor.visit_table(self)
    end

    def quote quoter
      quoter.table(name)
    end

    # Returns a table column.
    #
    def [] col_name
      Column.new(col_name, self)
    end

    # Returns a table alias. 
    #
    def as name
      TableAlias.new(self, name)
    end

    # Returns the identifier that designates the table uniquely in the query
    # as a prefix for column references. ("foo"."a")
    #
    def as_column_prefix quoter
      quote(quoter)
    end

    def inspect
      lisp_inspect(:table, name)
    end
  end
end