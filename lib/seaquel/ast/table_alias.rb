
module Seaquel::AST
  class TableAlias
    attr_reader :table
    attr_reader :name

    def initialize table, name
      @table = table
      @name = name
    end

    def [] col_name
      Column.new(col_name, self)
    end

    def visit visitor
      visitor.visit_table_alias table, name
    end

    def as_column_prefix quoter
      quoter.identifier(name)
    end
  end
end