
module Seaquel
  # Visits a statement AST and issues a statement. 
  class StatementGatherer
    extend Forwardable

    attr_reader :current_join

    def initialize statement, quoter
      @statement = statement
      @quoter = quoter
      @current_join = nil
    end

    def_delegator :@quoter, :table, :quote_table

    def visit_from parent, *args
      s.from.concat(args)

      continue(parent)
    end
    def visit_select parent
      continue(parent)
    end
    def visit_project parent, fields
      continue(parent)

      # Since the tree is processed in lifo order, we need to only apply the
      # very last project. 
      s.project.replace(fields)
    end
    def visit_order_by parent, list
      continue(parent)

      s.order_by.replace(list)
    end
    def visit_where parent, expression
      continue(parent)

      s.where.concat(expression)
    end

    def visit_update parent, table
      continue(parent)

      s.set_type(:update)
      s.set_target(table)
    end
    def visit_set parent, assign_list
      continue(parent)

      s.set.concat(assign_list)
    end

    def visit_join parent, tables
      continue(parent)

      @current_join = s.join(tables)
    end
    def visit_on(parent, exps)
      continue(parent)

      raise InvalidExpression, ".on without a .join encoutered" \
        unless current_join
      
      current_join.on(*exps)
    end

    def visit_group_by parent, list
      continue(parent)

      s.group_by.replace(list)
    end
    def visit_having parent, exps
      continue(parent)

      s.having.concat(*exps)
    end

    def visit_insert parent
      continue(parent)

      s.set_type(:insert)
    end
    def visit_into parent, table
      continue(parent)

      s.set_target(table)
    end
    def visit_values parent, values
      continue(parent)

      s.values << AST::List.new(values)
    end
    def visit_fields parent, fields
      continue(parent)

      s.fields.concat(fields)
    end

    def visit_delete parent
      continue(parent)

      s.set_type(:delete)
    end

    def visit_offset parent, n
      continue(parent)

      s.set_offset(n)
    end
    def visit_limit parent, n
      continue(parent)

      s.set_limit(n)
    end

  private
    # A shorthand for saying that node needs to be visited.
    #
    def continue node
      node.visit(self) if node
    end

    # A short-cut for the code in here.
    def s
      @statement
    end
  end
end