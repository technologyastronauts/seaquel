require_relative 'bit'

module Seaquel
  class ExpressionConverter
    extend Forwardable

    attr_reader :quoter

    def initialize quoter
      @quoter = quoter
    end

    # Gets called whenever the expression contains a statement-type AST::Node. 
    #
    def visit_node node
      bit(node.to_sql, 0)
    end

    def_delegator :@quoter, :string, :quote_string
    def_delegator :@quoter, :number, :quote_number

    def visit_assign left, right
      if left.kind_of? AST::Column
        bit(
          [left.as_column_reference(quoter), '=', sql(right).at(0)].join)
      else
        bit(
          [sql(left).at(0), '=', sql(right).at(0)].join)
      end
    end

    def visit_new_statement *args
      ::Seaquel::Generator.new(self).compact_sql
    end

    def visit_literal literal
      bit(literal)
    end

    def visit_list elements
      bit(elements.map { |e| sql(e).at(0) }.join(', '))
    end

    def visit_column_list elements
      # Column lists only contain columns.
      bit(elements.map { |e| e.as_column_reference(quoter) }.join(', '))
    end

    def visit_table table
      bit(table.quote(quoter))
    end

    def visit_table_alias table, name
      bit([
        sql(table).at(0), 
        "AS", 
        quoter.identifier(name)
        ].join(' '))
    end

    def visit_alias name, to
      prec = precedence(:as)
      bit("#{sql(to).at(prec)} AS \"#{name}\"")
    end

    def visit_binop op, left, right
      prec = precedence(op)

      if right.nil?
        op = :is if op==:eq
        op = :isnot if op==:noteq
      end

      if op==:in && right.kind_of?(Range)
        raise "Ranges excluding the right side are not permitted in Seaquel." \
          if right.exclude_end?

        return bit([
          sql(left).at(0), 
          'BETWEEN', 
          right.begin, 
          'AND', 
          right.end].join(' '))
      end

      if binops.has_key?(op)
        symbol = binops[op]

        return bit(
          [sql(left).at(prec), symbol, sql(right).at(prec)].join, 
          prec)
      end

      raise "No such operation (#{op.inspect})."
    end

    def visit_joinop op, exps
      # Shortcut if we join one element only. 
      if exps.size == 1
        el = exps.first
        return sql(el)
      end

      prec = precedence(op)
      parts = exps.map { |e| sql(e).at(prec) }

      sql = case op
        when :and
          parts.join(' AND ')
        when :or
          parts.join(' OR ')
      else
        raise "AF: JoinOp called for #{op.inspect}, but has no branch for it."
      end

      bit(sql, prec)
    end

    def visit_unary op, exp
      raise "No such unary operation #{op.inspect}." \
        unless unaryops.has_key?(op)

      symbol, prefix = unaryops[op]
      prec = precedences[op]

      parts = []

      parts << symbol if prefix 
      parts << sql(exp).at(0)
      parts << symbol unless prefix 

      bit(parts.join, prec)
    end

    def visit_column col
      bit(col.as_full_reference(quoter))
    end

    def visit_binding pos
      bit("$#{pos}")
    end

    def visit_funcall name, args
      arglist = args.map { |arg| sql(arg).toplevel }.join(', ')
      bit("#{name}(#{arglist})")
    end

    def visit_order order, expr
      bit("#{sql(expr).at(0)} #{order.upcase}")
    end

    def sql node
      # p [:sql, node]
      case node
        when nil
          bit('NULL')
        when Array, Set
          sql_bits = node.map { |el| sql(el).at(0) }
          bit("(" + sql_bits.join(', ') + ")")
        when String
          bit(quote_string(node))
        when Fixnum
          bit(quote_number(node))
        when true,false
          node ? bit('TRUE') : bit('FALSE')
      else
        node.visit(self)
      end
    end

  private
    def bit str, precedence=:inf
      Bit.new(str, precedence)
    end

    def precedence op
      precedences[op]
    end

    def binops
      @binops ||= {
        :eq   => '=',
        :gt   => '>', 
        :gteq => '>=', 
        :lt   => '<', 
        :lteq => '<=', 
        :is   => ' IS ', 
        :isnot => ' IS NOT ', 
        :noteq => '!=', 
        :in   => ' IN ', 
        :like => ' LIKE ', 
        :not_like => ' NOT LIKE ', 
        :ilike => ' ILIKE ', 
        :not_ilike => ' NOT ILIKE ', 
        :similar_to => ' SIMILAR TO ',
        :not_similar_to => ' NOT SIMILAR TO ', 
        :plus => '+', 
        :minus => '-', 
        :div => '/', 
        :times => '*', 
        :mod => '%', 
        :pow => '^', 
        :sqrt => '|/', 
        :cbrt => '||/',
        :bitand => '&', 
        :bitor => '|', 
        :bitxor => '#', 
        :sleft => '<<', 
        :sright => '>>'
      }
    end

    def unaryops
      @unaryops ||= {
                # sym,   prefix?
        :not => ['NOT ', true], 
        :bitnot => ['~', true],
        :fact => ['!', false],
      }
    end

    def precedences
      @precedences ||= begin
        prec = 1 # 0 reserved for simple values
        precs = Hash.new(0)

        assign = -> (*list) {
          list.map { |e| precs[e] = prec }
          prec += 1
        }
        
        # By listing something above something else, it gets a lower precedence
        # assigned. List from lower to higher precedences. Equivalence classes
        # by extending the argument list.
        assign[:not]
        assign[:as, :is, :isnot]
        assign[:or]
        assign[:and]
        assign[:eq, :gt, :gteq, :lt, :lteq]
        assign[:plus, :minus]
        assign[:times, :div]

        precs
      end
    end
  end
end