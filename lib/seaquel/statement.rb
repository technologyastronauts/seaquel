
require_relative 'statement/join'

module Seaquel

  # Gets raised when during the generation of SQL we discover that the statement
  # has no valid form. 
  #
  class InvalidStatement < StandardError
  end

  # Root node for an sql statement. 
  #
  class Statement

    attr_reader :expression_convertor

    attr_reader :from
    attr_reader :where
    attr_reader :set
    attr_reader :target
    attr_reader :values
    attr_reader :fields
    attr_reader :joins
    attr_reader :limit
    attr_reader :offset
    attr_reader :having
    
    # These are overwritten, not appended to. 
    attr_reader :project
    attr_reader :order_by
    attr_reader :group_by

    def initialize expression_convertor
      @expression_convertor = expression_convertor

      @from = list()
      @project = list()
      @order_by = list()
      @where = AST::JoinOp.new(:and, [])
      @set = list()
      @values = list()
      @fields = column_list()
      @joins = []
      @having = AST::JoinOp.new(:and, [])
      @group_by = list()
    end

    def set_limit n
      @limit = n
    end
    def set_offset n
      @offset = n
    end

    # Allows to determine the type of statement generated. 
    #
    # @param type [{:update, :select, :delete}]
    #
    def set_type type
      @type = type
    end

    def set_target table
      @target = table
    end

    # Produces a join clause and adds it to the joins list. 
    #
    # @param tables [Array<Object>] a list of tables to join to
    # @return [Join] Join subobject.
    #
    def join tables
      join = Join.new(tables)
      joins << join

      join
    end

    def to_s variant=:compact
      case @type 
        when :update
          generate_update(variant)
        when :insert
          generate_insert(variant)
        when :delete
          generate_delete(variant)
      else
        generate_select(variant)
      end
    end

    # Turns an expression into SQL
    #
    def convert exp
      expression_convertor.sql(exp).toplevel
    end
  private

    # Produces an empty list. 
    #
    def list
      AST::List.new
    end
    def column_list
      AST::ColumnList.new
    end

    # Wraps str in parenthesises.
    #
    # Example: 
    #   parens('foo') # => '(foo)'
    def parens str
      "(#{str})"
    end

    # Generates SQL for a DELETE statement. 
    # 
    def generate_delete variant
      parts = []
      parts << "DELETE"

      unless from.empty?
        parts << 'FROM'
        parts << convert(from)
      end

      unless joins.empty?
        joins.each do |join|
          parts << join.convert(
            self # as #convert
          )
        end
      end

      unless where.empty?
        parts << 'WHERE'
        parts << convert(where)
      end

      parts.join(' ')      
    end

    # Generates SQL for an INSERT statement. 
    # 
    def generate_insert variant
      parts = []
      parts << "INSERT"

      if target
        parts << "INTO"
        parts << convert(target)
      end

      unless values.empty?
        if fields.empty?
          # If you are inserting data to all the columns, the column names can be
          # omitted.
          parts << 'VALUES'
          parts << values.
            map { |value_list| 
              parens(convert(value_list)) }.
            join(', ')
        else
          # A field list must be produced
          # assert: !values.empty? && !fields.empty?

          parts << parens(convert(fields))
          parts << 'VALUES'
          parts << values.
              map { |value_list| 
                raise InvalidStatement, "Field list in INSERT statement doesn't match value list (#{fields.size} fields and #{value_list.size} values)." \
                  unless fields.size == value_list.size

                parens(convert(value_list)) }.
              join(', ')
        end
      end

      parts.join(' ')
    end

    # Generates SQL for a SELECT statement. 
    #
    def generate_select variant
      parts = []
      parts << "SELECT"

      if project.empty?
        parts << '*'
      else
        parts << convert(project)
      end

      unless from.empty?
        parts << 'FROM'
        parts << convert(from) 
      end

      unless joins.empty?
        joins.each do |join|
          parts << join.convert(
            self # as #convert
          )
        end
      end

      unless where.empty?
        parts << 'WHERE'
        parts << convert(where)
      end

      unless group_by.empty?
        parts << 'GROUP BY'
        parts << convert(group_by)
      end

      unless having.empty?
        parts << 'HAVING'
        parts << convert(having)
      end

      unless order_by.empty?
        parts << 'ORDER BY'
        parts << convert(order_by)
      end

      if offset
        parts << 'OFFSET'
        parts << convert(offset)
      end

      if limit 
        parts << 'LIMIT'
        parts << convert(limit)
      end

      parts.join(' ')
    end

    # Generates SQL for an UPDATE statement.
    #
    def generate_update variant 
      parts = []
      parts << 'UPDATE'
      parts << convert(target)

      unless set.empty?
        parts << 'SET'
        parts << convert(set)
      end

      unless where.empty?
        parts << 'WHERE'
        parts << convert(where)
      end

      parts.join(' ')
    end
  end
end