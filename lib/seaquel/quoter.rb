module Seaquel
  class Quoter
    def table name
      identifier(name)
    end

    def column name
      %Q("#{name}")
    end

    def string str
      "'" + str.gsub("'", "''") + "'"
    end

    def number num
      num.to_s
    end

    def identifier name
      %Q("#{name}")
    end
  end
end