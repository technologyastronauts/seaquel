
require 'forwardable'

require 'seaquel/expression_converter'

module Seaquel
  class Generator
    def initialize ast
      @ast = ast
    end

    def compact_sql
      quoter = Quoter.new
     
      # Construct a statement 
      expression_converter = ExpressionConverter.new(quoter)
      statement = Statement.new(expression_converter)

      # And a visitor for the AST
      visitor = StatementGatherer.new(statement, quoter)

      # Gather statement details from the AST
      @ast.visit(visitor)

      # Turn the statement into SQL.
      statement.to_s(:compact)
    end
  end
end