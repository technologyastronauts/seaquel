

module Seaquel

  # Represents a small bit of an SQL expression. 
  #
  class Bit
    attr_reader :str
    attr_reader :precedence

    def initialize str, precedence
      @str = str
      @precedence = precedence
    end

    # Returns a string at given target_precedence. If the precedence of this 
    # bit is lower than target_precedence, it will not be put in parenthesis.
    #
    def at target_precedence
      if precedence == :inf || target_precedence <= precedence
        @str
      else
        "(#{@str})"
      end
    end

    # Returns the SQL that you would insert toplevel, meaning at a level where
    # parens aren't needed anymore. 
    #
    def toplevel
      str
    end

    def == other
      return toplevel == other if other.kind_of?(String)

      super
    end
  end
end